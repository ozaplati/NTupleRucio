#!/usr/bin/python
import os, sys

#outputBasePath = "/eos/home-o/ozaplati/TEST/test_rucio_NTuple_dwn/Data18_STDM11_TEST/v6/EMTopoJets/"


####################################
# Data18_v6_02_09_2020 - EMTopoJets
#rucioDatasetName = "user.ozaplati.02_09_2020_Data18_v6"
#outputBasePath   = "/eos/home-o/ozaplati/TEST/test_rucio_NTuple_dwn/Data18_v6_02_09_2020_disk_space/"

####################################
# Data17_v6_02_09_2020 - EMTopoJets
#rucioDatasetName = "user.ozaplati.02_09_2020_Data17_v6"
#outputBasePath   = "/eos/home-o/ozaplati/TEST/test_rucio_NTuple_dwn/Data17_v6_02_09_2020_disk_space/"

####################################
# Data16_v6_03_08_2020
#rucioDatasetName = "user.ozaplati.03_08_2020_Data16_v6"
#outputBasePath   = "/eos/home-o/ozaplati/TEST/test_rucio_NTuple_dwn/Data16_03_08_2020_disk_space/"


####################################
# Data15_v6_03_08_2020
#rucioDatasetName = "user.ozaplati.03_08_2020_Data15_v6"
#outputBasePath   = "/eos/home-o/ozaplati/TEST/test_rucio_NTuple_dwn/Data15_03_08_2020_disk_space/"


####################################
# Data17_5TeV_lowMu_v6_02_09_2020 - not validated - EMTopoJets
#rucioDatasetName = "user.ozaplati.02_09_2020_Data17_5TeV_lowMu_v6"
#outputBasePath   = "/eos/home-o/ozaplati/TEST/test_rucio_NTuple_dwn/Data17_5TeV_lowMu_v6_02_09_2020_disk_space/"


####################################
# 29_09_2020_Data18_PFlow_v8
#rucioDatasetName = "user.ozaplati.29_09_2020_Data18_PFlow_v8"
#outputBasePath   = "/eos/home-o/ozaplati/IJXS/NTuples/v8/rucio_disk_space/Data18_PFlow_v8_29_09_2020_disk_space/"

###################################
# Data16_v7 29_09_2020
#rucioDatasetName = "user.ozaplati.29_09_2020_Data16_v7"
#outputBasePath   = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data16/disk_space_Data16_v7_29_09_2020/"

####################################
# Data17_v7 29_09_2020
#rucioDatasetName = "user.ozaplati.29_09_2020_Data17_v7"
#outputBasePath   = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data17/disk_space_Data17_v7_29_09_2020/"

#############################################
# Data15_v7 - 29_09_2020
#rucioDatasetName = "user.ozaplati.29_09_2020_Data15_v7"
#outputBasePath = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data15/disk_space_Data15_v7_29_09_2020/"


#############################################
# Data15 v7 - 29_09_2020
# FZU EMTopoJets
# 
#rucioDatasetName = "user.ozaplati.29_09_2020_Data15_v7"
#outputBasePath = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data15/EMTopoJets/disk_space/rucio/"

#############################################
# Data16 v7 - 29_09_2020
# FZU EMTopoJets
# 
#rucioDatasetName = "user.ozaplati.29_09_2020_Data16_v7"
#outputBasePath = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data16/EMTopoJets/disk_space/rucio/"

#############################################
# Data17 v7 - 29_09_2020
# FZU EMTopoJets
# 
#rucioDatasetName = "user.ozaplati.29_09_2020_Data17_v7"
#outputBasePath = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data17/EMTopoJets/disk_space/rucio/"

#############################################
# Data18 v7 - 29_09_2020
# FZU EMTopoJets
# 
#rucioDatasetName = "user.ozaplati.29_09_2020_Data18_v7"
#outputBasePath = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data18/EMTopoJets/disk_space/rucio/"

rucioDatasetName = "user.ozaplati.25_11_2020_Data15_PFlow_v10"
outputBasePath = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data15/PFlowJets/disk_space/rucio/txt/"

dic_subDirs = {
	"cutflow"   : "_cutflow.root",
	"metadata"  : "_metadata.root",
	"log"       : ".log",
	"ANALYSIS"  : "_ANALYSIS.root",
	"duplicates": "_duplicates_tree.root",
}



##################################################################################
## DO NOT MODIFY
##################################################################################
pwdPath = os.getcwd()

os.system("mkdir -p " + outputBasePath)

for key in dic_subDirs:
	subDir = key
	subNameContainer = dic_subDirs[key]
	
	# create subDir
	outDir = outputBasePath #+ "/" + subDir
	os.system("mkdir -p " + outDir)
	
	logDir_rucio = outputBasePath + "/" + "rucio_log" + "/" + subDir
	os.system("mkdir -p " + logDir_rucio)
	
	# make a file with all containers
	outContainerList = logDir_rucio + "/" + key + "_list.txt"
	os.system("rucio list-dids " + rucioDatasetName + "*" + subNameContainer + " --short " + ">" + outContainerList)
	
	l_containers = list()
	
	# load a outContainerList to l_containers
	with open(outContainerList) as f:
		line = f.readline()
		line_counter = 1
		while line:
			# remove end of line
			container = line.strip()
			print("Line {}: {}".format(line_counter, container))
			l_containers.append(container)
			
			# load new line
			line = f.readline()
			line_counter += 1
	
	# loop over all containers in a list
	dic_ = {}
	for container in l_containers:
		print ("\n\n\n")
		print ("//////////////////////////////"*3)
		print ("//    Working on: ")
		print ("//    " + container)
		print ("//")
		os.chdir(outDir)
		logFile_rucio_log = logDir_rucio + "/" + container
		
		dic_.update({container : logFile_rucio_log})
		cmd_rucio = "rucio list-files " + container + " 2>&1 | tee " + logFile_rucio_log
		print (cmd_rucio)
		os.system(cmd_rucio)
	
	l_sizes = list()
	for key in dic_:
		with open(dic_[key]) as f: 
			lines = f.readlines() 
			for line in lines:
				if line == lines[-1]:
					l_sizes.append(key + "\t" + line.strip())
					print("last line: " + line.strip() +  "\t " + key)
	
	l_sizes = sorted(l_sizes)
	with open(outDir + "/table_" + subDir + ".txt", 'w') as f_out:
		for item in l_sizes:
			f_out.write('%s\n' % item)

os.chdir(pwdPath)

print("\n\nDONE!")


