#!bin/python
import os

########################################################################
#                     Setup part of the script                         #
########################################################################


#############################################
# Data18_PFlow_v8 - 29_09_2020
# FZU
#inputBasePath  = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v8/Data18/PFlowJets/"
#outputBasePath = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v8/Data18/PFlowJets/disk_space/fs/"

#############################################
# Data15_v7 - 29_09_2020
# FZU
#inputBasePath  = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data15/EMTopoJets/"
#outputBasePath = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data15/EMTopoJets/disk_space/fs/"

#############################################
# Data16_v7 - 29_09_2020
# FZU
#inputBasePath  = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data16/EMTopoJets/"
#outputBasePath = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data16/EMTopoJets/disk_space/fs/"

#############################################
# Data17_v7 - 29_09_2020
# FZU
#inputBasePath  = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data17/EMTopoJets/"
#outputBasePath = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data17/EMTopoJets/disk_space/fs/"


#############################################
# Data18_v7 - 29_09_2020
# FZU
inputBasePath  = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data18/EMTopoJets/"
outputBasePath = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data18/EMTopoJets/disk_space/fs/"


dic_subDirs = {
	"cutflow"   : "_cutflow.root",
	"metadata"  : "_metadata.root",
	"log"       : ".log",
	"ANALYSIS"  : "_ANALYSIS.root",
	"duplicates": "_duplicates_tree.root",
}


########################################################################
#                      Main part of the script                         #
########################################################################

os.system("mkdir -p " + outputBasePath)

for key in dic_subDirs:
	subDir = key
	subNameContainer = dic_subDirs[key]
	
	# get working directory
	dirName = inputBasePath + "/" + subDir
	
	# get lits of all directories - e.q. various Data periods
	lFiles = os.listdir(inputBasePath + "/" + subDir)
	
	# alphabetical order
	lFiles = sorted(lFiles)
	
	# remove ouput file if alredy exists
	outputFile = outputBasePath + "/size_" + subDir + ".txt"
	if os.path.isfile(outputFile):
		os.remove(outputFile)
	
	# look over all relevant directories e.q. Data periods
	for iFile in lFiles:
		# call bash function to evaluate size of file/directory
		#    common  du -sh command is based on       1MB = 1024 kB
		#    however rucio size of file is based on   1MB = 1000 kB
		#    
		#    script bash/du_file.sh applies the same base units as rucio
		#       it means 1MB = 1000 kB
		#
		#    call source bash/du_file.sh <file>
		
		command = "source " + os.environ["NTUPLE_RUCIO"] + "/bash/du_file.sh " + dirName + "/" + iFile
		command += " 2>&1 | tee -a " + outputFile
		os.system(command)
