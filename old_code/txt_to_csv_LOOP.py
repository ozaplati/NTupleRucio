#bin/python
import os, sys
sys.path.append("python/CSV")
from txt_to_csv import txt_to_list
from txt_to_csv import list_to_csv

########################################################################
# Setup part
inputFile  = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data15/PFlowJets/disk_space/rucio/txt/"
outputFile = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data15/PFlowJets/disk_space/rucio/csv/"

l_subDirs = [
	"cutflow",
	"metadata",
	"log",
	"ANALYSIS",
	"duplicates",
]


########################################################################
## DO NOT MODIFY
########################################################################

for subDir in l_subDirs:
	
	#input_txt_file  = inputFile +  "/" + subDir + "/" + "table_" + subDir + ".txt"
	#output_csv_file = outputFile + "/" + subDir + "/" + "table_" + subDir + ".csv"
	#output_csv_dir  = outputFile + "/" + subDir 

	input_txt_file  = inputFile +  "/" + "table_" + subDir + ".txt"
	output_csv_file = outputFile + "/" + "table_" + subDir + ".csv"
	output_csv_dir  = outputFile + "/" 
	
	
	####################################################################
	# create output_csv_dir
	os.system("mkdir -p " + output_csv_dir)
	
	
	####################################################################
	# The script for one file
	list_lines = list()
	list_lines = txt_to_list(input_txt_file)
	
	selected_columns = [0, 4, 5]
	list_lines_selected = list()
	list_lines_selected = [["file", "size", "unit"]]
	for line in list_lines:
		i_line = list()
		for i in range(len(line)):
			if i in selected_columns:
				i_line.append(line[i])
		list_lines_selected.append(i_line)
	
	list_to_csv(list_lines_selected, output_csv_file)
	#list_to_csv(list_lines, outputFile)


