#bin/python

########################################################################
# Setup part
inputFile  = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data15/PFlowJets/disk_space/rucio/ANALYSIS/size_ANALYSIS.txt"
outputFile = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data15/PFlowJets/disk_space/rucio/ANALYSIS/test.csv"


########################################################################
# The script
import os, sys
sys.path.append("python/CSV")
from txt_to_csv import txt_to_list
from txt_to_csv import list_to_csv

list_lines = list()
list_lines = txt_to_list(inputFile)
list_to_csv(list_lines, outputFile, [0, 4, 5])
#list_to_csv(list_lines, outputFile)


