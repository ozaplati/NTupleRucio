#!bin/python

########################################################################
#                         Setup part                                   #
########################################################################

# path to input file - lxplus and fzu
file_1 = "/eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data18_STDM11/v7/EMTopoJets/disk_space/csv/lxplus/table_ANALYSIS.csv"
file_2 = "/eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data18_STDM11/v7/EMTopoJets/disk_space/csv/fzu/table_ANALYSIS.csv"

file_output="merge.csv"

########################################################################
#                            Script                                    #
########################################################################

import os, sys
sys.path.append("python/CSV")
from csv_table import MergeTable

MergeTable(file_1, file_2, file_output)
