#!bin/python
########################################################################
#                            Setup                                     #
########################################################################


#############################################
# Data18_v7 - 29_09_2020
# FZU
inputBasePath1 = "/eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data18_STDM11/v7/EMTopoJets/disk_space/csv/lxplus/"
inputBasePath2 = "/eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data18_STDM11/v7/EMTopoJets/disk_space/csv/fzu/"
outputPath     = "/eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data18_STDM11/v7/EMTopoJets/disk_space/csv/merge/"

l_tableNames = [
	"table_ANALYSIS.csv",
	"table_metadata.csv",
	"table_cutflow.csv",
	"table_duplicates.csv",
]


########################################################################
#                            Script                                    #
########################################################################
import os, sys
os.system("mkdir -p " + outputPath)

sys.path.append("python/CSV")
from csv_table import MergeTable

for table in l_tableNames:
	file1  = inputBasePath1 + "/" + table
	file2  = inputBasePath2 + "/" + table
	output = outputPath + "/" + table
	
	MergeTable(file1, file2, output)
