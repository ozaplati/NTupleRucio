#!bin/python

# import pandas module - works with csv file
import pandas as pd

def MergeTable(_file_csv_1, _file_csv_2, _file_csv_output):
	
	""" merge 2 scv tables together"""
	
	# load csv files - columns are separated by ";"
	df1 = pd.read_csv(_file_csv_1, sep=";")
	df2 = pd.read_csv(_file_csv_2, sep=";")
	
	
	# merge 2 csv files together
	df_combined = pd.concat([df1, df2],axis=1)
	
	df1_n_col = len(df1.axes[1])
	df2_n_col = len(df2.axes[1])
	
	ind_new_col = df1_n_col + df2_n_col
	
	# insert ratio column
	df_combined.insert(ind_new_col, "ratio", df1['size']/df2['size'])
	
	
	# round ratio column 
	df_combined["ratio"].round(4)
	
	
	# save new csv file as file_output
	df_combined.to_csv(_file_csv_output, index=None)
	print(df_combined)
