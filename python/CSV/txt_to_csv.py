#!bin/python

def txt_to_list(_inputFile):
	""" 
	syntax:
	    function txt_to_list(_inputFile)
	    _inputFile .... path to an input line
	    
	description:
	    convert input file to list of list of strings,
	    it means list of strings per one line in a file
	"""
	
	l_items = list() 
	with open(_inputFile, 'r') as inputFile:
		for line in inputFile:
			# debug prints
			#print line
			#print "\n\n"
			l_toSave = list()
			if line[0] == '#':
				continue
			else:
				for item in line.split():
					#print "\t\t" + item
					l_toSave.append(item)
			
			l_items.append(l_toSave)
	print(l_items)
	return(l_items)

def list_to_csv(_list_of_lines, _output_file, _l_select_colums=None):
	""" 
	syntax:
	    function list_to_csv(_list_of_lines, _output_file, _l_select_colums=None)
	    _list_of_lines   .... list(list(string))
	                     .... like list of lines 
	                     .... and  line of words
	    _output_file     .... path to output csv file
	    _l_select_colums .... list of columns which you want to consider
	                     .... None as default - it means let's consider all columns
	                     
	description:
	    convert input _list_of_lines to output csv file _output_file
	"""
	
	if _l_select_colums is None:
		_l_select_colums = []
	
	# generate table as csv file
	with open(_output_file, 'w') as out_file:
		# loop over lines in _list_of_lines
		for line in _list_of_lines:
			i_line = ""
			# loop over words in one line
			for i in range(len(line)):
				# item as a word
				item = line[i]
				# if it is an considered column
				if len(_l_select_colums) == 0 or (len(_l_select_colums) != 0 and i in _l_select_colums):
					print(item)
					i_line += item + ";"
				
				# if end of line
				# or a comment then continue
				if item == line[-1] or item[0] == "#":
					i_line += "\n"
					continue

			print i_line
			out_file.write(i_line)


