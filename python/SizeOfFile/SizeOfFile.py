#!bin/python

import enum
# Enum for size units
class SIZE_UNIT(enum.Enum):
   BYTES = 1
   KB = 2
   MB = 3
   GB = 4

def convert_unit(size_in_bytes, unit):
   """ Convert the size from bytes to other units like KB, MB or GB"""
   if unit == SIZE_UNIT.KB:
       return size_in_bytes/1024.0
   elif unit == SIZE_UNIT.MB:
       return size_in_bytes/(1024.0*1024.0)
   elif unit == SIZE_UNIT.GB:
       return size_in_bytes/(1024.0*1024.0*1024.0)
   else:
       return size_in_bytes


def convert_rucio_unit(size_in_bytes, unit):
   """ Convert the size from bytes to other units like KB, MB or GB"""
   if unit == SIZE_UNIT.KB:
       return size_in_bytes/1000.0
   elif unit == SIZE_UNIT.MB:
       return size_in_bytes/(1000.0*1000.0)
   elif unit == SIZE_UNIT.GB:
       return size_in_bytes/(1000.0*1000.0*1000.0)
   else:
       return size_in_bytes
       
import os
def get_file_size(file_name, size_type = SIZE_UNIT.BYTES ):
   """ Get file in size in given unit like KB, MB or GB"""
   size = os.path.getsize(file_name)
   return convert_unit(size, size_type)

def get_file_rucio_size(file_name, size_type = SIZE_UNIT.BYTES ):
   """ Get file in size in given unit like KB, MB or GB"""
   size = os.path.getsize(file_name)
   return convert_rucio_unit(size, size_type)
   
   
#file_path = '/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data18/EMTopoJets/ANALYSIS/user.ozaplati.29_09_2020_Data18_v7.periodB_ANALYSIS.root/user.ozaplati.22730892._000001.ANALYSIS.root'

# get file size in KB
#size = get_file_size(file_path, SIZE_UNIT.BYTES)
#print('Size of file is : ', size ,  'KB')
