#bin/python
import os, sys
sys.path.append("../CSV/")
from txt_to_csv import txt_to_list
from txt_to_csv import list_to_csv


def txt_to_csv_loop(_basePathInputFiles, _l_subDirs, _basePathOutputFiles):
	"""
	function txt_to_csv_loop(_basePathInputFiles, _l_txtFiles, _basePathOutputFiles)
	
	arguments:
		_basePathInputFiles  .... base path of input txt files
		_l_subDirs           .... list of all secific part of names of considered txt files 
		_basePathOutputFiles .... output path of new csv 
	description:
		apply txt_to_csv funtion in a loop on all txt files
	"""
	
	########################################################################
	## DO NOT MODIFY
	########################################################################
	
	for subDir in _l_subDirs:
		
		#input_txt_file  = _basePathInputFiles +  "/" + subDir + "/" + "table_" + subDir + ".txt"
		#output_csv_file = _basePathOutputFiles + "/" + subDir + "/" + "table_" + subDir + ".csv"
		#output_csv_dir  = _basePathOutputFiles + "/" + subDir 
		
		input_txt_file  = _basePathInputFiles +  "/" + "table_" + subDir + ".txt"
		output_csv_file = _basePathOutputFiles + "/" + "table_" + subDir + ".csv"
		output_csv_dir  = _basePathOutputFiles + "/" 
		
		
		####################################################################
		# create output_csv_dir
		os.system("mkdir -p " + output_csv_dir)
		
		
		####################################################################
		# The script for one file
		list_lines = list()
		list_lines = txt_to_list(input_txt_file)
		
		selected_columns = [0, 4, 5]
		list_lines_selected = list()
		list_lines_selected = [["file", "size", "unit"]]
		for line in list_lines:
			i_line = list()
			for i in range(len(line)):
				if i in selected_columns:
					i_line.append(line[i])
			list_lines_selected.append(i_line)
		
		list_to_csv(list_lines_selected, output_csv_file)
		#list_to_csv(list_lines, _basePathOutputFiles)


# Example
#basePathInputFiles  = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data16/PFlowJets/disk_space/rucio/txt/"
#basePathOutputFiles = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data16/PFlowJets/disk_space/rucio/csv/"
#
#l_subDirs = [
#	"cutflow",
#	"metadata",
#	"ANALYSIS",
#	"duplicates",
#]
#
#txt_to_csv_loop(basePathInputFiles, l_subDirs, basePathOutputFiles)
