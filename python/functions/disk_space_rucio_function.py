#!/usr/bin/python
import os, sys


def disk_space_rucio_to_txt(_rucioDataSetBaseName, _outputBasePath, _dic_subDirs):
	"""
	function disk_space_rucio_to_txt(_rucioDataSetBaseName, _outputBasePath, _dic_subDirs)
	
	arguments:
		_rucioDataSetBaseName .... string variable 
		                      .... key string in the name of rucio container
		_outputBasePath       .... string variable
		                      .... output path, there will be created new directory structure based on last argument of _dic_subDirs
		_dic_subDirs          .... dictionary keies and values, both keies and values are strings
		                      .... key .... sub-directory to save txt file
		                               .... the final txt file fill be saved at <_outputBasePath>/<key>
		                      .... val .... name of directory inside of rucio container .... like cutflow.root or metadata.root
		                      .... example:
		                                   dic_subDirs = { "cutflow" : "_cutflow.root" }
	description:
		dump txt table from rucio database
		
		in more precise words function looks into a rucio database on all containers named as <_rucioDataSetBaseName>
		function iterates over all files of interest <_rucioDataSetBaseName>*<dic_subDirs.val>
		and save a txt file at at <_outputBasePath>/<key>
		each output txt file include table of all rucio files inside rucio container _rucioDataSetBaseName>*<dic_subDirs.val> and its size as well
	"""
	##################################################################################
	## DO NOT MODIFY
	##################################################################################
	pwdPath = os.getcwd()
	
	os.system("mkdir -p " + _outputBasePath)
	
	for key in _dic_subDirs:
		subDir = key
		subNameContainer = _dic_subDirs[key]
		
		# create subDir
		outDir = _outputBasePath #+ "/" + subDir
		os.system("mkdir -p " + outDir)
		
		logDir_rucio = _outputBasePath + "/" + "rucio_log" + "/" + subDir
		os.system("mkdir -p " + logDir_rucio)
		
		# make a file with all containers
		outContainerList = logDir_rucio + "/" + key + "_list.txt"
		os.system("rucio list-dids " + _rucioDataSetBaseName + "*" + subNameContainer + " --short " + ">" + outContainerList)
		
		l_containers = list()
		
		# load a outContainerList to l_containers
		with open(outContainerList) as f:
			line = f.readline()
			line_counter = 1
			while line:
				# remove end of line
				container = line.strip()
				print("Line {}: {}".format(line_counter, container))
				l_containers.append(container)
				
				# load new line
				line = f.readline()
				line_counter += 1
		
		# loop over all containers in a list
		dic_ = {}
		for container in l_containers:
			print ("\n\n\n")
			print ("//////////////////////////////"*3)
			print ("//    Working on: ")
			print ("//    " + container)
			print ("//")
			os.chdir(outDir)
			logFile_rucio_log = logDir_rucio + "/" + container
			
			dic_.update({container : logFile_rucio_log})
			cmd_rucio = "rucio list-files " + container + " 2>&1 | tee " + logFile_rucio_log
			print (cmd_rucio)
			os.system(cmd_rucio)
		
		l_sizes = list()
		for key in dic_:
			with open(dic_[key]) as f: 
				lines = f.readlines() 
				for line in lines:
					if line == lines[-1]:
						l_sizes.append(key + "\t" + line.strip())
						print("last line: " + line.strip() +  "\t " + key)
		
		l_sizes = sorted(l_sizes)
		with open(outDir + "/table_" + subDir + ".txt", 'w') as f_out:
			for item in l_sizes:
				f_out.write('%s\n' % item)
	
	os.chdir(pwdPath)
	print("\n\nDONE!")

# example to use
#rucioDatasetName = "user.ozaplati.25_11_2020_Data15_PFlow_v10"
#outputBasePath = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data15/PFlowJets/disk_space/rucio/txt/"
#
#dic_subDirs = {
#	"cutflow"   : "_cutflow.root",
#	"metadata"  : "_metadata.root",
#	"log"       : ".log",
#	"ANALYSIS"  : "_ANALYSIS.root",
#	"duplicates": "_duplicates_tree.root",
#}
#
#disk_space_rucio_to_txt(rucioDatasetName, outputBasePath, dic_subDirs)
