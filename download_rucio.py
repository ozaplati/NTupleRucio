#!/usr/bin/python
import os, sys

########################################
# Data18_v6_02_09_2020 - EMTopoJets
#outputBasePath   = "/eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data18_STDM11/v6/EMTopoJets/"
#rucioDatasetName = "user.ozaplati.02_09_2020_Data18_v6"

########################################
# Data17_v6_02_09_2020 - EMTopoJets
#rucioDatasetName = "user.ozaplati.02_09_2020_Data17_v6"
#outputBasePath   = "/eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data17_STDM11/v6/EMTopoJets/"

####################################
# Data16_v6_03_08_2020 - EMTopoJets
rucioDatasetName = "user.ozaplati.03_08_2020_Data16_v6"
outputBasePath   = "/eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data16_STDM11/v6/EMTopoJets/"

####################################
# Data15_v6_03_08_2020 - EMTopoJets
#rucioDatasetName = "user.ozaplati.03_08_2020_Data15_v6"
#outputBasePath   = "/eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data15_STDM11/v6/EMTopoJets/"


dic_subDirs = {
	"cutflow"   : "_cutflow.root",
	"metadata"  : "_metadata.root",
	"log"       : ".log",
	"ANALYSIS"  : "_ANALYSIS.root",
	"duplicates": "_duplicates_tree.root",
}



##################################################################################
## DO NOT MODIFY
##################################################################################
pwdPath = os.getcwd()

os.system("mkdir -p " + outputBasePath)

for key in dic_subDirs:
	subDir = key
	subNameContainer = dic_subDirs[key]
	
	# create subDir
	outDir = outputBasePath + "/" + subDir
	os.system("mkdir -p " + outDir)
	
	logDir_rucio = outputBasePath + "/" + "rucio_log" + "/" + subDir
	os.system("mkdir -p " + logDir_rucio)
	
	# make a file with all containers
	outContainerList = logDir_rucio + "/" + key + "_list.txt"
	os.system("rucio list-dids " + rucioDatasetName + "*" + subNameContainer + " --short " + ">" + outContainerList)
	
	l_containers = list()
	
	# load a outContainerList to l_containers
	with open(outContainerList) as f:
		line = f.readline()
		line_counter = 1
		while line:
			# remove end of line
			container = line.strip()
			print("Line {}: {}".format(line_counter, container))
			l_containers.append(container)
			
			# load new line
			line = f.readline()
			line_counter += 1
	
	# loop over all containers in a list
	for container in l_containers:
		print ("\n\n\n")
		print ("//////////////////////////////"*3)
		print ("//    Working on: ")
		print ("//    " + container)
		print ("//")
		os.chdir(outDir)
		logDir_rucio_dwn = logDir_rucio + "/log"
		os.system("mkdir -p " + logDir_rucio_dwn)
		cmd_dwn = "rucio -v download " + container + " 2>&1 | tee " + logDir_rucio_dwn + "/log_" + container
		print (cmd_dwn)
		os.system(cmd_dwn)

os.chdir(pwdPath)

print("\n\nDONE!")


