#!bin/python
########################################################################
#                            Setup                                     #
########################################################################

########################################################################
# Data15_v10 - PFlow - 25_11_2020
#rucioDatasetName = "user.ozaplati.25_11_2020_Data15_PFlow_v10"
#outputBasePath   = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data15/PFlowJets/disk_space/rucio"

########################################################################
# Data16_v10 - PFlow - 25_11_2020
#rucioDatasetName = "user.ozaplati.25_11_2020_Data16_PFlow_v10"
#outputBasePath   = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data16/PFlowJets/disk_space/rucio"

########################################################################
# Data17_v10 - PFlow - 25_11_2020
#rucioDatasetName = "user.ozaplati.25_11_2020_Data17_PFlow_v10"
#outputBasePath   = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data17/PFlowJets/disk_space/rucio"

########################################################################
# Data18_v10 - PFlow - 25_11_2020
rucioDatasetName = "user.ozaplati.25_11_2020_Data18_PFlow_v10"
outputBasePath   = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data18/PFlowJets/disk_space/rucio"


dic_subDirs = {
	"cutflow"   : "_cutflow.root",
	"metadata"  : "_metadata.root",
	"ANALYSIS"  : "_ANALYSIS.root",
	"duplicates": "_duplicates_tree.root",
}



########################################################################
#                            Script                                    #
########################################################################

# dump txt table from rucio database
import os, sys
sys.path.append("python/functions")
from disk_space_rucio_function import disk_space_rucio_to_txt

outputBasePath_txt = outputBasePath + "/txt"
disk_space_rucio_to_txt(rucioDatasetName, outputBasePath_txt, dic_subDirs)

# convert txt table to csv table
sys.path.append("python/CSV")
sys.path.append("python/functions")
from txt_to_csv_function import txt_to_csv_loop

basePathInputFiles_txt  = outputBasePath_txt
basePathOutputFiles_csv = outputBasePath + "/csv"
l_subDirs = dic_subDirs.keys()
txt_to_csv_loop(basePathInputFiles_txt, l_subDirs, basePathOutputFiles_csv)

