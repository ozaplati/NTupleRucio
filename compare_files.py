#!bin/python

#from os import listdir
#from os.path import isfile, join
import os

def GetAllFilesRecursively(_PATH):
	# get list of directories in _PATH
	l_dirs = os.listdir(_PATH)
	
	# loop over all directories in _PATH
	files = [];
	for iDir in l_dirs:
		print 
		path = _PATH + "/" + iDir
		
		if os.path.isdir(path):
			# path is a directory
			# -> get all files in this directory
			#    and save it to files list
			l_1 = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]
			files.extend(l_1)
		elif os.path.isfile(path):
			# path is a file
			# -> get this file
			#    and save it to files list
			files.append(path)
	return(files)


def Replace_function(_l_input, _l_key_string):
	l = list()
	for i in range(len(_l_input)):
		item = _l_input[i]
		print ("\n\n")
		for i_replace in _l_key_string:
			if i_replace in item:
				print ("\t" + i_replace + " in " + item)
				item =  item.replace(i_replace, "")
				_l_input[i] = item
				print ("\t new " + _l_input[i])
				#print files_1[i]
	return(_l_input)

path_1 = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data17/EMTopoJets/ANALYSIS"
path_2 = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v7/Data17/EMTopoJets/log"

l_replace_path1 = [
	"user.ozaplati.",
	"29_09_2020_Data17_v7",
	"ANALYSIS",
	".root",
	"_",
]

l_replace_path2 = [
	"user.ozaplati.",
	"29_09_2020_Data17_v7",
	"log.tgz",
	"log.",
	"period",
	"B", "C", "D", "E", "F", "G", "H", "I", "J", "K",
	"..",
]

files_1 = GetAllFilesRecursively(path_1)
files_2 = GetAllFilesRecursively(path_2)

all_files = files_1 
all_files.extend(files_2)

files_1_id = Replace_function(files_1, l_replace_path1)
files_2_id = Replace_function(files_2, l_replace_path2)

id_missing = list()

print "files_1_id"
for i in files_1_id:
	if (i not in files_2_id):
		print "\t " + i
		id_missing.append(i)

print "files_2_id"
for i in files_2_id:
	if (i not in files_1_id):
		print "\t" + i
		id_missing.append(i)

print "size files_1: " + str(len(files_1))
print "size files_2: " + str(len(files_2))


for i_missing in id_missing:
	l_ids = i_missing.split(".")
	print l_ids
	
	for i_in_all in all_files:
		if l_ids[0] in i_in_all and l_ids[1] in all_files:
			print "missing file: "  + i_missing


