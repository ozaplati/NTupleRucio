#bin/python
import glob
import os, sys

sys.path.append("python/SizeOfFile")
from SizeOfFile import SIZE_UNIT
from SizeOfFile import get_file_size
from SizeOfFile import get_file_rucio_size

from SizeOfFile import convert_unit
from SizeOfFile import convert_rucio_unit

#############################################
# Data15_v10 - PFlow - 25_11_2020
#inputBasePath = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data15/PFlowJets/"
#outputPath    = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data15/PFlowJets/disk_space/system/csv/"

#############################################
# Data16_v10 - PFlow - 25_11_2020
#inputBasePath = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data16/PFlowJets/"
#outputPath    = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data16/PFlowJets/disk_space/system/csv/"

#############################################
# Data17_v10 - PFlow - 25_11_2020
#inputBasePath = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data17/PFlowJets/"
#outputPath    = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data17/PFlowJets/disk_space/system/csv/"

#############################################
# Data18_v10 - PFlow - 25_11_2020
inputBasePath = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data18/PFlowJets/"
outputPath    = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data18/PFlowJets/disk_space/system/csv/"


#dir_file_mode = "file"
dir_file_mode = "dir"

dic_subDirs = {
	"cutflow"   : {"dir_or_file": dir_file_mode, "suffix": ".root", "unit" : SIZE_UNIT.KB},
	"metadata"  : {"dir_or_file": dir_file_mode, "suffix": ".root", "unit" : SIZE_UNIT.KB},
	"log"       : {"dir_or_file": dir_file_mode, "suffix": ".log",  "unit" : SIZE_UNIT.KB},
	"ANALYSIS"  : {"dir_or_file": dir_file_mode, "suffix": ".root", "unit" : SIZE_UNIT.GB},
	"duplicates": {"dir_or_file": dir_file_mode, "suffix": ".root", "unit" : SIZE_UNIT.KB},
}

########################################################################
#                      Main part of the script                         #
########################################################################

os.system("mkdir -p " + outputPath)

for key in dic_subDirs:
	# load input dictionary
	subDir = key
	suffix = dic_subDirs[key]["suffix"]
	unit_default  = dic_subDirs[key]["unit"]
	unit = unit_default
	dir_or_file = dic_subDirs[key]["dir_or_file"]
	
	if dir_or_file not in ["dir", "file"]:
		msg = "ERROR: wron value of dir_or_file variable. \n"
		msg += "\t use only \"dir\" or \"file\""
		raise (msg)
	
	do_file = False
	do_dir = False
	
	if dir_or_file == "dir":
		# dir mode
		do_file = False
		do_dir = True
	else:
		# file mode
		do_file = True
		do_dir = False
	
	# get working directory
	path_subDir = inputBasePath + "/" + subDir
	
	# get all files in path_subDir recursively
	#    with appropriate suffix
	
	lFiles = []
	# r=root, d=directories, f = files
	for r, d, f in os.walk(path_subDir):
		print "r: " + str(r) + "\n"
		print "d: " + str(d) + "\n"
		print "f: " + str(f) + "\n"
		#raw_input("stop")
		if do_file == True:
			for file in f:
				if suffix in file:
					lFiles.append(os.path.join(r, file))
		else:
			if suffix in r:
				lFiles.append(r) # [ path_subDir + "/" + i_directory for i_directory in d ]
	print lFiles
	#raw_input ("stop")
	
	# alphabetical order of files
	lFiles = sorted(lFiles)
	
	l_table = list()
	for iFile in lFiles:
		size = 0
		if do_file == True:
			# this is a file mode
			# do_file == True
			# do_dir  == False
			size = get_file_rucio_size(iFile, unit)
			if size < 1.0:
				size_tmp_GB = get_file_rucio_size(iFile, SIZE_UNIT.GB)
				size_tmp_MB = get_file_rucio_size(iFile, SIZE_UNIT.MB)
				size_tmp_KB = get_file_rucio_size(iFile, SIZE_UNIT.KB)
				# choose suitable units
				if size_tmp_GB > 1.0:
					size = size_tmp_GB
					unit = SIZE_UNIT.GB
				elif size_tmp_MB > 1.0:
					size = size_tmp_MB
					unit = SIZE_UNIT.MB
				else:
					size = size_tmp_KB
					unit = SIZE_UNIT.KB
			
		else:
			# this is a dir mode
			# do_dir  == True
			# do_file == False
			unit = unit_default
			
			for f in glob.glob(iFile + "/*" + suffix):
				i_size = get_file_rucio_size(f, unit)
				size += i_size
			
			if size < 1.0:
				# eveluate size of directory as a sum of all files with approptiate suffix
				# evaluta site of file in GB, MB an KB
				size_tmp_GB = 0
				size_tmp_MB = 0
				size_tmp_KB = 0
				for f in glob.glob(iFile + "/*" + suffix):
					i_size_GB = get_file_rucio_size(f, SIZE_UNIT.GB)
					size_tmp_GB += i_size_GB
				for f in glob.glob(iFile + "/*" + suffix):
					i_size_MB = get_file_rucio_size(f, SIZE_UNIT.MB)
					size_tmp_MB += i_size_MB
				for f in glob.glob(iFile + "/*" + suffix):
					i_size_KB = get_file_rucio_size(f, SIZE_UNIT.KB)
					size_tmp_KB += i_size_KB
				
				# choose suitable units
				if size_tmp_GB > 1.0:
					size = size_tmp_GB
					unit = SIZE_UNIT.GB
				elif size_tmp_MB > 1.0:
					size = size_tmp_MB
					unit = SIZE_UNIT.MB
				else:
					size = size_tmp_KB
					unit = SIZE_UNIT.KB
			
			
		str_size = str(size)
		str_unit = str(unit).replace("SIZE_UNIT.", "")
		
		item = (iFile, str_size, str_unit)
		
		l_table.append(item)
	
	output_file = outputPath + "/" + "table_" + subDir  + ".csv"
	
	# generate table as csv file
	with open(output_file, 'w') as out_file:
		names_line = "file" + ";" + "size" + ";" + "unit" + "\n"
		out_file.write(names_line)
		
		for line in l_table:
			i_file = line[0]
			i_size = line[1]
			i_unit = line[2]
			
			i_line = i_file + ";" + str(round(float(i_size), 3)) + ";" + i_unit + "\n"
			
			print i_line
			out_file.write(i_line)
