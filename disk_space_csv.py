#bin/python
import glob
import os, sys

sys.path.append("python/SizeOfFile")
from SizeOfFile import SIZE_UNIT
from SizeOfFile import get_file_size
from SizeOfFile import convert_unit


#############################################
# Data18_v7 - 29_09_2020
#inputBasePath  = "/eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data18_STDM11/v7/EMTopoJets/"
#outputPath     = "/eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data18_STDM11/v7/EMTopoJets/disk_space/csv/lxplus"

#############################################
# Data17_v7 - 29_09_2020
#inputBasePath  = "/eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data17_STDM11/v7/EMTopoJets/"
#outputPath     = "/eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data17_STDM11/v7/EMTopoJets/disk_space/csv/lxplus"

#############################################
# Data16_v7 - 29_09_2020
#inputBasePath  = "/eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data16_STDM11/v7/EMTopoJets/"
#outputPath     = "/eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data16_STDM11/v7/EMTopoJets/disk_space/csv/lxplus"

#############################################
# Data15_v7 - 29_09_2020
inputBasePath  = "/eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data15_STDM11/v7/EMTopoJets/"
outputPath     = "/eos/atlas/atlascerngroupdisk/phys-sm/incljets2019/Data15_STDM11/v7/EMTopoJets/disk_space/csv/lxplus"


dic_subDirs = {
	"cutflow"   : {"suffix": ".root", "unit" : SIZE_UNIT.KB},
	"metadata"  : {"suffix": ".root", "unit" : SIZE_UNIT.KB},
	"log"       : {"suffix": ".log",  "unit" : SIZE_UNIT.KB},
	"ANALYSIS"  : {"suffix": ".root", "unit" : SIZE_UNIT.GB},
	"duplicates": {"suffix": ".root", "unit" : SIZE_UNIT.KB},
}


########################################################################
#                      Main part of the script                         #
########################################################################

os.system("mkdir -p " + outputPath)

for key in dic_subDirs:
	# load input dictionary
	subDir = key
	suffix = dic_subDirs[key]["suffix"]
	unit   = dic_subDirs[key]["unit"]
	
	# get working directory
	path_subDir = inputBasePath + "/" + subDir
	
	# get all files in path_subDir recursively
	#    with appropriate suffix
	
	lFiles = []
	# r=root, d=directories, f = files
	for r, d, f in os.walk(path_subDir):
		for file in f:
			if suffix in file:
				lFiles.append(os.path.join(r, file))
	
	# alphabetical order of files
	lFiles = sorted(lFiles)
	
	l_table = list()
	for iFile in lFiles:
		
		size = get_file_size(iFile, unit)
		str_size = str(size)
		str_unit = str(unit).replace("SIZE_UNIT.", "")
		
		item = (iFile, str_size, str_unit)
		
		l_table.append(item)
	
	output_file = outputPath + "/" + "table_" + subDir  + ".csv"
	
	# generate table as csv file
	with open(output_file, 'w') as out_file:
		names_line = "file" + ";" + "size" + ";" + "unit" + "\n"
		out_file.write(names_line)
		
		for line in l_table:
			i_file = line[0]
			i_size = line[1]
			i_unit = line[2]
			i_line = i_file + ";" + i_size + ";" + i_unit + "\n"
			
			print i_line
			out_file.write(i_line)
