#!bin/bash
FILE=${1}

du -Lsb ${FILE} | awk '
     function du_file(bytes) {
         hum[1000**4]="TiB";
         hum[1000**3]="GiB";
         hum[1000**2]="MiB";
         hum[1000]="kiB";
         for (x = 1000**4; x >= 1000; x /= 1000) {
             if (bytes >= x) {
                 return sprintf("%8.3f %s", bytes/x, hum[x]);
             }
         }
         return sprintf("%4d     B", bytes);
     }
 
     {
         print du_file($1) "\t" $2
     }
'
