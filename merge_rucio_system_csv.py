#!bin/python
########################################################################
#                            Setup                                     #
########################################################################

#############################################
# Data15_v10 - 25_11_2020
#inputBasePath1 = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data15/PFlowJets/disk_space/rucio/csv/"
#inputBasePath2 = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data15/PFlowJets/disk_space/system/csv/"
#outputPath     = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data15/PFlowJets/disk_space/"

#############################################
# Data16_v10 - 25_11_2020
#inputBasePath1 = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data16/PFlowJets/disk_space/rucio/csv/"
#inputBasePath2 = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data16/PFlowJets/disk_space/system/csv/"
#outputPath     = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data16/PFlowJets/disk_space/"

#############################################
# Data17_v10 - 25_11_2020
#inputBasePath1 = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data17/PFlowJets/disk_space/rucio/csv/"
#inputBasePath2 = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data17/PFlowJets/disk_space/system/csv/"
#outputPath     = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data17/PFlowJets/disk_space/"

#############################################
# Data18_v10 - 25_11_2020
inputBasePath1 = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data18/PFlowJets/disk_space/rucio/csv/"
inputBasePath2 = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data18/PFlowJets/disk_space/system/csv/"
outputPath     = "/mnt/nfs19/zaplatilek/IJXS/NTuples/v10/Data18/PFlowJets/disk_space/"


l_tableNames = [
	"table_ANALYSIS.csv",
	"table_metadata.csv",
	"table_cutflow.csv",
	"table_duplicates.csv",
]


########################################################################
#                            Script                                    #
########################################################################
import os, sys

os.system("mkdir -p " + outputPath)

sys.path.append("python/CSV")
#import pandas
from csv_table import MergeTable

for table in l_tableNames:
	file1  = inputBasePath1 + "/" + table
	file2  = inputBasePath2 + "/" + table
	output = outputPath + "/" + table
	
	MergeTable(file1, file2, output)
