# NTupleRucio

** Download IJXS NTuples from rucio database**

# Download the package

`git clone https://gitlab.cern.ch/ozaplati/NTupleRucio.git`


# Setup
`source setup.sh`

# Run - Check size of NTuples in rucio database
1. edit configuration part of `disk_space_rucio.py`:
	  - rucioDatasetName .... specific sub-name of rucio dataset container
      - outputBasePath ........ output base path
2.  run as:
    - `python disk_space_rucio.py`


# Run - download NTuples from rucio database
1. edit configuration part of `download_rucio.py`:
	  - rucioDatasetName .... specific sub-name of rucio dataset container
      - outputBasePath ........ output base path at IJXS atlas cern group disk
      - dic_subDirs ........... sub-directories inside the rucio container
2.  run as:
    - `python download_rucio.py`


# Run - Check size of NTuples at local file system/disk
1. edit configuration part of `disk_space_system.py`:
	  - inputBasePath .... input base path at local file system
      - outputPath ........ output base path
      - dir_file_mode ..... use "dir"  for csv table of whole directories in the rucio container
                      ..... use "file" for csv table of all lines in the rucio container
     - dic_subDirs    ..... dictionary which specify considered dir_file_mode, file with appropriate suffix, units of disk size
                      ..... e.q.:
                      dic_subDirs = {
                                       "cutflow"   : {"dir_or_file": dir_file_mode, "suffix": ".root", "unit" : SIZE_UNIT.KB}
                                    }
2. run as:
    - `python disk_space_system.py`


# Merge csv tables of rucio and system for comparison of data completness
1. edit configuration part of `merge_rucio_system_csv.py`
    - inputBasePath1 ..... base-path to rucio  csv files (produced by `disk_space_rucio.py`)
    - inputBasePath2 ..... base-path to system csv files (produced by `disk_space_system.py`)
    - outputPath  ........ output base-path
    - l_tableNames ....... list of csv file-names to be compared, the same names for rucio and syste are expected


2.  run as:
    - `python disk_space_system.py`

# pandas module to merge csv files
you can have a problem with pandas module when you are running `python merge_rucio_system_csv.py`

if the pandas module is not available for you then you should apply the following commands
```
cd python/csv
pip install virtualenv
virtualenv venv
source venv/bin/activate
pip install pandas
```
